/*		
	AUBIH
	Functions js
	
	Copyright (c) 2015 AUBIH
	71000, Sarajevo
	Bosnia and Hercegovina
	+387.63.697.680
	http://http://aubih.edu.ba/
	info@aubih.edu.ba/
	
	This program is copyrighted; you can't redistribute it and/or
	modify it under any circumstances.
	All rights reserved.
	
	Revision
	No.		User					Date			Description
	=================================================================================================================
	1.		Dragomir Vučković		03.03.2015		Creation of functions javascript page
*/

/* Mail crypt */
(function($){
  $.fn.mailcrypt = function(options) {
    return this.each(function() {
		at = "@";
		$(":first-child", this).replaceWith(at);
		email = $(this).html();
		$(this).attr("href", "mailto:" + email);
    });
  };
})(jQuery);

/* Declare variables and constants */
var loader = 500;
var homehashtag = "#Welcome to American University in Bosnia and Hercegovina";
var lang = document.cookie.replace(/(?:(?:^|.*;\s*)lang\s*\=\s*([^;]*).*$)|^.*$/, "$1");
var slider_gs;
var slider_interval = 10000;
var slideInterval;
var anim = 0;
var screen_width = $(window).width();
var screen_height = $(window).height();

/* Start loader */
var start_loader = function() {	
	$("body").removeClass("loaded");
	return false;	
}

/* Stop loader */
var stop_loader = function(loader) {	
	setTimeout(function() {
		$("body").addClass("loaded");      
	}, loader);
	
	return false;	
}

/* Email validation */
var validateEmail = function(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    } else {
        return false;
    }
}

/* Colleges on hover - random ids */
var coll_random = function(coll, coll_array) {
	var coll_r = "";
	var i = 0;
	
	while (i < 11) {
		coll_r = coll_array[Math.floor((Math.random() * coll_array.length))];
		if (coll_r != "" && coll_r != coll) {
			break;
		}
		i++;
	}
	
	var a = coll_array.indexOf(coll_r);
	coll_array.splice(a, 1);	
	
	return coll_r;
} 

/* Download file */
var download_file = function(obscure) {	
	$.ajax({
		type: "POST",
		url: "modules/config/functions/download-link.php",
		data: {
			obscure: obscure },
		dataType: "json",
		success: function(data) {	
			var url = "modules/config/functions/download-header.php?fname=" + data.fname + "&ftype=" + data.ftype + "&flink=" + data.flink + "";		
			$("body").append("<iframe id=\"download\" src=\"" + url + "\" style=\"display: none;\"></iframe>");
			setTimeout('$("#download").remove()', 5000);				
	}});	
}

/* Random number */
var random_number = function(lbound, ubound) {
	return (Math.floor(Math.random() * (ubound - lbound)) + lbound);
}

/* Random charachers */
var random_char = function(number, lower, upper, other) {
	var number_chars = "0123456789";
	var lower_chars = "abcdefghijklmnopqrstuvwxyz";
	var upper_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	var other_chars = "!@#$%&*()-_=+;:',.?";
	var char_set = "";
	
	if (number == true) {
		char_set += number_chars;
	}
	if (lower == true) {
		char_set += lower_chars;
	}
	if (upper == true) {
		char_set += upper_chars;
	}
	if (other == true){
		char_set += other_chars;
	}
	
	return char_set.charAt(random_number(0, char_set.length));
}

/* Random string */
var random_string = function(length, number, lower, upper, other) {
	var random_str = "";
	
	for (i = 0; i < length; i++){
		random_str = random_str + random_char(number, lower, upper, other);
	}
	
	return random_str;
}

/* Slider */
var slider = function(parent, child, zindex) {
	var active = $(parent + " .active");
	var next = ($(parent + " .active").next().length > 0) ? $(parent + " .active").next() : $(parent + " " + child + ":first");
	
	next.css("z-index", zindex - 1);
	active.fadeOut(function() {
		active.css("z-index", 1).show().removeClass("active");
		next.css("z-index", zindex).addClass("active");
	});
  
	return false;
}
var stop_slider = function() {
	clearInterval(slider_gs);
	return false;
}
var start_slider = function(parent, child, zindex) {
	$(parent + " .active").css("z-index", zindex);
	
	if(slider_gs) {
		stop_slider();
	}
	
	slider_gs = setInterval(function() { 
		slider(parent, child, zindex); 
	}, slider_interval);
	
	return false;
}

/* Slider */
var slide_content = function(start) {	
	var video = 0;
		
	if (start == "yes") {
		clearInterval(slideInterval);
		slideInterval = setInterval(function(){
			if (anim == 0) {
				anim = 1;
				
				$("#main-content-wraper").animate({ marginLeft: -(screen_width) }, 800, function() {
					$(this).css({marginLeft:0}).find("div.slide-box:last").after($(this).find("div.slide-box:first"));
					if ($(this).find("div.slide-box:first").attr("id") == "slide1") {
						$("#download-brochure").hide();
						clearInterval(slide_content);
            			slide_content("no");
					} else {
						$("#download-brochure").show();
					}
					anim = 0;
				});			
			}
		}, slider_interval);		
	} else {
		clearInterval(slideInterval);
	}
}

/* Video slide */
var slide_video_content = function() {
	var video = document.getElementById("home-video");
	video.currentTime = 0;
	video.play();
	var init_video = setInterval(function(){
		if (video.ended) {
			$("#main-content-wraper").animate({ marginLeft: -(screen_width) }, 800, function() {
				$(this).css({marginLeft:0}).find("div.slide-box:last").after($(this).find("div.slide-box:first"));
				slide_content("yes");
			});			
			clearInterval(init_video);
		}
	}, 1000);
}