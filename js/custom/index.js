$(function () {
    $(document).ready(function () {
        var $navToggle = $("#navToggle");
        var getHeight = function() {
            return $(window).scrollTop();
        };

        var positionNavbarOnTop = function () {
            $navToggle.css("top", "0");
            $(".animated-arrow").css("margin-top", "0px");
        };

        if (getHeight() > 0) {
            positionNavbarOnTop();
        }

        $(window).scroll(function () {
            if (getHeight() > 0) {
                positionNavbarOnTop();
            } else {
                $navToggle.css("top", "48px");
            }
        });

        var $equalHeight = $(".equal-height");
        if($equalHeight.length > 0) $equalHeight.responsiveEqualHeightGrid();
    });
});